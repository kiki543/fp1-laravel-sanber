<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('dashboard', 'DashboardController');
    Route::resource('slider', 'SliderController');
    Route::resource('kategori', 'KategoriController');
    Route::resource('postingan', 'PostinganController');
});

// Route::get('/login', 'HomeController@login');
Route::get('/logout', 'HomeController@logout');

Route::get('/', 'BlogController@index');
Route::get('/detail/{slug}', 'BlogController@detail')->name('frontend.detail');
Route::get('/kontak', 'BlogController@kontak')->name('frontend.kontak');
Route::post('/detail/{slug}', 'BlogController@tampil')->name('frontend.detail');
Route::get('/cari', 'BlogController@cari')->name('frontend.cari');
