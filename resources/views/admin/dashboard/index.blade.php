@extends('backend.konten')

@section('judul', 'Dashboard')

@section('isikonten')
<div class="container">
  <div class="row">
    <div class="col-md-12 mt-2">
      <div class="alert alert-primary" role="alert">
        <h5>Selamat Datang <b style="text-transform: uppercase">{{Auth::user()->name}}</b></h5>
      </div>
    </div>
    <div class="col-md-6">
      <div class="info-box">
        <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Kategori</span>
          <span class="info-box-number">{{$kategori->count()}}</span>
          <div class="ml-auto">
            <a href="{{route('kategori.index')}}" class="btn btn-info btn-sm">Kategori</a>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="info-box">
        <span class="info-box-icon bg-primary"><i class="far fa-envelope"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Slider</span>
          <span class="info-box-number">{{$slider->count()}}</span>
          <div class="ml-auto">
            <a href="{{route('slider.index')}}" class="btn btn-primary btn-sm">Slider</a>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="info-box">
        <span class="info-box-icon bg-success"><i class="far fa-envelope"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Postingan</span>
          <span class="info-box-number">{{$postingan->count()}}</span>
          <div class="ml-auto">
            <a href="{{route('postingan.index')}}" class="btn btn-success btn-sm">Postingan</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection