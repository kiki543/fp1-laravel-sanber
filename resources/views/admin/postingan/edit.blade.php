@extends('backend.konten')

@section('judul', 'Edit Postingan')

@section('isikonten')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('postingan.update', $postingan->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="formGroupExampleInput">Edit Judul Postingan</label>
                            <input type="text" class="form-control" value="{{$postingan->judul}}" name="judul" id="formGroupExampleInput" placeholder="Masukkan Judul Postingan">
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput">Edit Deskripsi</label>
                            <textarea type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Masukkan Deskripsi">{{$postingan->deskripsi}}</textarea>
                        </div>
                        <div class="form-group">
                            <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Edit Kategori</label>
                            <select class="custom-select my-1 mr-sm-2" name="kategori_id" id="inlineFormCustomSelectPref">
                                <option selected>Pilih Kategori</option>
                                @foreach ($kategori as $item)
                                <option value="{{$item->id}}" @if ($postingan->kategori_id == $item->id)
                                    selected
                                    @endif
                                    >{{$item->nama_kategori}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="formGroupExampleInput">Edit Tag Postingan</label>
                            <input type="text" class="form-control" value="{{$postingan->tag}}" name="tag" id="formGroupExampleInput" placeholder="Masukkan Judul Postingan">
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlFile1">File Gambar</label>
                            <input type="file" class="form-control-file" id="exampleFormControlFile1" name="gambar">
                        </div>

                        <div class="form-group">
                            <img src="{{asset($postingan->gambar)}}" width="30%" height="30%">
                        </div>
                        <a href="{{route('postingan.index')}}" class="btn btn-primary btn-sm">Kembali</a>
                        <button type="submit" class="btn btn-success btn-sm">Simpan postingan</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>


<script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'deskripsi' );
</script>
@endsection