@extends('backend.konten')

@section('judul', 'Tambah Postingan')

@section('isikonten')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('postingan.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="formGroupExampleInput">Tambahkan Judul postingan</label>
                            <input type="text" class="form-control" name="judul" id="formGroupExampleInput" placeholder="Masukkan Judul Postingan">
                        </div>

                        <div class="form-group">
                            <label for="formGroupExampleInput">Tambahkan Deskripsi</label>
                            <textarea class="form-control" name="deskripsi" id="deskripsi" placeholder="Masukkan Deskripsi"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="formGroupExampleInput">Tambahkan Kategori yang Berkaitan</label>
                            <select class="form-control select2 select2-hidden-accessible" name="kategori_id" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                <option selected="selected" data-select2-id="">Pilih Kategori</option>
                                @foreach ($kategori as $item)
                                <option data-select2-id="{{$item->id}}" value="{{$item->id}}">{{$item->nama_kategori}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="formGroupExampleInput">Tambahkan Tag postingan</label>
                            <input type="text" class="form-control" name="tag" id="formGroupExampleInput" placeholder="Masukkan Tag Postingan">
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlFile1">File Gambar</label>
                            <input type="file" class="form-control-file" id="exampleFormControlFile1" name="gambar">
                        </div>
                        <a href="{{route('postingan.index')}}" class="btn btn-primary btn-sm">Kembali</a>
                        <button type="submit" class="btn btn-success btn-sm">Simpan postingan</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>


<script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'deskripsi' );
</script>

@endsection