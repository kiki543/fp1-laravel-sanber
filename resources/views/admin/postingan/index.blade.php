@extends('backend.konten')

@section('judul', 'postingan')

@section('isikonten')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-2">
            <div class="card mt-2">
                <div class="card-header">
                    <a href="{{route('postingan.create')}}" class="btn btn-primary">Tambah postingan</a>
                </div>
                <div class="card-body">

                    {{-- Kategori yang disimpan hanya bisa disimpan 1 kali --}}
                    <table class="table table-bordered">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Judul</th>
                                <th scope="col">Deskripsi</th>
                                <th scope="col">Kategori</th>
                                <th scope="col">Tag</th>
                                <th scope="col">Slug</th>
                                <th scope="col">Penulis</th>
                                <th scope="col">Gambar</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($postingan as $item => $hasil)
                            <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                <td>{{$hasil->judul}}</td>
                                <td>{!!substr($hasil->deskripsi, 0, 25) !!}</td>
                                <td><em>#{{$hasil->kategori->nama_kategori}}</em></td>
                                <td> {{$hasil->tag}}
                                </td>
                                <td><em>{{$hasil->slug}}</em></td>
                                <td>{{$hasil->users->name}}</td>
                                <td width="15%"><img src="{{asset( $hasil->gambar )}}" alt="{{$hasil->judul}}" style="width:150px;"></td>
                                <td width="10%">
                                    <form action="{{route('postingan.destroy',$hasil->id)}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <a href="{{route('postingan.edit', $hasil->id)}}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                        <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{$postingan->links()}}
                </div>
            </div>
        </div>
    </div>
</div>

@include('sweetalert::alert')

@endsection