@extends('backend.konten')

@section('judul', 'Kategori')

@section('isikonten')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-2">
            <div class="card mt-2">
                <div class="card-header">
                    <a href="{{route('kategori.create')}}" class="btn btn-primary">Tambah Kategori</a>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama Kategori</th>
                                <th scope="col">Slug</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($kategori as $item)
                            <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                <td>{{$item -> nama_kategori}}</td>
                                <td>{{$item -> slug}}</td>
                                <td width="15%">

                                    <form action="{{route('kategori.destroy',$item->id)}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <a href="{{route('kategori.edit', $item->id)}}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                        <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    {{$kategori->links()}}
                </div>
            </div>
        </div>
    </div>
</div>

@include('sweetalert::alert')

@endsection