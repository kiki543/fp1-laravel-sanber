@extends('backend.konten')

@section('judul', 'Tambah Kategori')

@section('isikonten')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('kategori.store')}}" method="POST">
                    @csrf
                        <div class="form-group">
                            <label for="formGroupExampleInput">Tambahkan Kategori</label>
                            <input type="text" class="form-control" name="nama_kategori" id="formGroupExampleInput" placeholder="Masukkan Nama Kategori">
                        </div>
                        <a href="{{route('kategori.index')}}" class="btn btn-primary btn-sm">Kembali</a>
                        <button type="submit" class="btn btn-success btn-sm">Simpan Kategori</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>

@endsection