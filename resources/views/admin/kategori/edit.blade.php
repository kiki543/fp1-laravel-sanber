@extends('backend.konten')

@section('judul', 'Edit Kategori')

@section('isikonten')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-2">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('kategori.update',$kategori->id)}}" method="POST">
                    @csrf
                    @method('put')
                        <div class="form-group">
                            <label for="formGroupExampleInput">Edit Kategori</label>
                            <input type="text" class="form-control" value="{{$kategori->nama_kategori}}" name="nama_kategori" id="formGroupExampleInput" placeholder="Masukkan Nama Kategori">
                        </div>
                        <a href="{{route('kategori.index')}}" class="btn btn-primary btn-sm">Kembali</a>
                        <button type="submit" class="btn btn-success btn-sm">Simpan Kategori</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>

@endsection