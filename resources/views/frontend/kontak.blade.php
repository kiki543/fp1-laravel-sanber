@extends('webfront.konten')

@section('judul', 'About')

@section('isi')

<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4 text-center">Kontak Kami</h1>
  </div>
</div>

<div class="container mb-4">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <h5 class="card-header">AABlogging</h5>
        <div class="card-body">
          <h5 class="card-title">Bila mempunyai berita untuk disebarkan, bisa kontak kami melalui</h5>
          <ul class="list-group">
            <li class="list-group-item">Andi Rizky Wijaya -> <a href="https://gitlab.com/kiki543">Kunjungi</a></li>
            <li class="list-group-item">I Gde Bayu Priyambada Marayasa -> <a href="https://gitlab.com/bayupm">Kunjungi</a></li>
            <li class="list-group-item">Akhmad Rizqi Azhari -> <a href="https://gitlab.com/RizqiAzhari">Kunjungi</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection