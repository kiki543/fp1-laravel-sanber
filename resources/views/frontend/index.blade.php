@extends('webfront.konten')

@section('judul','Tempatnya berita terpopuler')


@section('isi')
<div class="row">
  <div class="col-md-12">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        @foreach ($slider as $item)
        <div class="carousel-item {{$loop->first ? 'active' : ''}}">
          <img class="d-block w-100 " height="500px" src="{{asset($item->gambar)}}">
        </div>
        @endforeach
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <!-- Blog Entries Column -->
    <div class="col-md-8">

      <h1 class="my-4">Berita Terkini
      </h1>

      <!-- Blog Post -->
      @foreach ($postingan as $item)
      <div class="card mb-4">
        <img class="card-img-top" src="{{asset($item->gambar)}}" height="250px">
        <div class="card-body">
          <h2 class="card-title">{{$item->judul}}</h2>
          <p class="card-text">{!!substr($item->deskripsi, 0 ,250)!!}</p>
          <a href="{{route('frontend.detail', $item->slug)}}" class="btn btn-primary btn-sm"> Detail &rarr;</a>
        </div>
        <div class="card-footer text-muted">
          <span class="badge badge-secondary">{{$item->kategori->nama_kategori}}</span> {{$item->created_at->diffForHumans()}} by
          <a href="#">{{$item->users->name}}</a>
        </div>
      </div>
      @endforeach

      {{$postingan->links()}}

    </div>

    <!-- Sidebar Widgets Column -->
    <div class="col-md-13">

      <!-- Search Widget -->
      <!-- Categories Widget -->
      <div class="card my-4">
        <h5 class="card-header">Categories</h5>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-6">
              <ul class="list-unstyled mb-0">
                @foreach ($kategori as $item)
                <li>
                  <h6 class="btn btn-dark btn-sm">{{$item->nama_kategori}} <span class="badge badge-light">{{$item->postingan->count()}}</span></h6>
                </li>
                @endforeach
            </div>
            </ul>
          </div>
        </div>
      </div>

      <!-- Side Widget -->
      <div class="card my-4">
        <h5 class="card-header">Tips dari penulis</h5>
        <div class="card-body">
          <ul>
            <li>
              Tetap belajar walaupun terasa sulit
            </li>
            <li>
              Berhenti yaa.. dia tidak bisa kau miliki
            </li>
            <li>
              Buanglah mimpimu dan matilah..
            </li>
          </ul>
        </div>
      </div>

    </div>
  </div>

</div>


@endsection