@extends('webfront.konten')

@section('judul', 'Detail Postingan')

@section('isi')

<div class="container">
  <div class="row">


    <!-- Blog Entries Column -->
    <div class="col-md-8 my-4">

      <!-- Blog Post -->
      @foreach ($postingan as $item)

      <img src="{{asset($item->gambar)}}" width="100%">
      <span class="btn btn-dark btn-sm mt-2" style="font-size: 12px">{{$item->kategori->nama_kategori}}</span>
      <h3 class="text-uppercase">{{$item->judul}}</h3>
      <p>{!!$item->deskripsi!!}</p>
      <hr>
      Telah posting : <span style="font-size: 15px; font-weight:bold; font-family:sans-serif; ">{{$item->created_at->diffForHumans()}} - {{$item->users->name}}</span>
      @endforeach


    </div>

    <!-- Sidebar Widgets Column -->
    <div class="col-md-4">

      <!-- Categories Widget -->
      <div class="card my-4">
        <h5 class="card-header">Categories</h5>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-6">
              <ul class="list-unstyled mb-0">
                @foreach ($kategori as $item)
                <li>
                  <h6 class="btn btn-dark btn-sm">{{$item->nama_kategori}} <span class="badge badge-light">{{$item->postingan->count()}}</span></h6>
                </li>
                @endforeach
            </div>
            </ul>
          </div>
        </div>
      </div>

      <!-- Side Widget -->
      <div class="card my-4">
        <h5 class="card-header">Jangan Lupa Belajar</h5>
        <div class="card-body">
          <ul>
            <li>
              Tetap belajar walaupun terasa sulit
            </li>
            <li>
              Berhenti yaa.. dia tidak bisa kau miliki
            </li>
            <li>
              Buanglah mimpimu dan matilah..
            </li>
          </ul>
        </div>
      </div>

    </div>

  </div>
</div>


{{-- <div class="container my-2">
  <div class="row">
    <div class="col-sm-8">
      <div class="card">
        <div class="card-header">
          Berikan Komentar
        </div>
      <div class="card-body">
      <form action="" method="post">
      @csrf
        <input type="hidden" name="id">
        <input type="hidden" name="postingan_id">
        <div class="form-group">
          <label for="exampleInputEmail1">Nama Anda</label>
          <input type="text" name="nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Komentar Anda</label>
          <input type="text" name="komen" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
      </div>
    </div>
    </div>

    <div class="container my-2">
      <div class="row">
        <div class="col-sm-8">
          <div class="card">
            <div class="card-header">
              Jawaban Anda
            </div>
            <div class="card-boy p-2">
            @foreach ($komen as $item)
                <h5>{{$item->nama}}</h5>
{{$item->komen}}
@endforeach

</div>
</div>

</div>
</div>
</div> --}}


</div>
</div>
</div>

@endsection