<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slider = Slider::latest()->paginate(10);
        return view('admin.slider.index', compact('slider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'nama_gambar'   => 'required|min:3',
                'gambar'        => 'required|image|mimes:jpg,png,jpeg|max:2048'
            ],
            [
                'nama_gambar.required' => 'Nama Slider minimal 3 huruf',
            ]
        );

        $gambar = $request->gambar;

        $gmbr_slider = time() . $gambar->getClientOriginalName();

        $slider = Slider::create([
            'nama_gambar' => $request->nama_gambar,
            'gambar' => 'public/uploads/slider/' . $gmbr_slider
        ]);

        $gambar->move('public/uploads/slider/', $gmbr_slider);

        return redirect()->route('slider.index')->with('toast_success', 'Slider Berhasil di tambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('admin.slider.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_gambar'   => 'required|min:3'
        ]);

        $slider = Slider::findorfail($id);

        if ($request->has('gambar')) {
            $gmbr_slider = $request->gambar;
            $gambar_slider_baru = time() . $gmbr_slider->getClientOriginalName();
            $gmbr_slider->move('public/uploads/slider/', $gambar_slider_baru);

            $slider_data = [
                'nama_gambar' => $request->nama_gambar,
                'gambar' => 'public/uploads/slider/' . $gambar_slider_baru
            ];
        } else {
            $slider_data = [
                'nama_gambar' => $request->nama_gambar,
            ];
        }

        $slider->update($slider_data);

        return redirect()->route('slider.index')->with('toast_success', 'Slider Berhasil di edit!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::destroy($id);

        return redirect()->route('slider.index')->with('toast_success', 'Slider Berhasil di hapus!');
    }
}
