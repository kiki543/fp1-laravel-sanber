<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Postingan;
use App\Kategori;
use Illuminate\Support\Facades\Auth;

class PostinganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // lakukan join agar menampilkan field slug pada tabel kategori dengan tabel postingan
        $kategori = Kategori::all();
        $postingan = Postingan::latest()->paginate(10);
        return view('admin.postingan.index', compact('kategori', 'postingan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        return view('admin.postingan.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul'         => 'required',
            'deskripsi'     => 'required',
            'kategori_id'   => 'required',
            'tag'           => 'required',
            'gambar'        => 'required|image|mimes:jpg,png,jpeg|max:2048'
        ]);

        $gambar = $request->gambar;

        $gambar_post = time() . $gambar->getClientOriginalName();

        $postingan = Postingan::create([
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
            'kategori_id' => $request->kategori_id,
            'tag' => $request->tag,
            'slug' => Str::slug($request->judul),
            'gambar' => 'public/uploads/post/' . $gambar_post,
            'users_id' => Auth::id()
        ]);

        $gambar->move('public/uploads/post/', $gambar_post);

        return redirect()->route('postingan.index')->with('toast_success', 'Postingan Berhasil di tambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // lakukan join agar menampilkan field nama_kategori pada tabel kategori dengan tabel postingan
        $postingan = Postingan::find($id);
        $kategori = Kategori::all();
        return view('admin.postingan.edit', compact('postingan', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'judul' => 'required',
            'deskripsi' => 'required',
            'kategori_id' => 'required',
            'tag' => 'required'

        ]);
        $postingan = Postingan::findorfail($id);

        if ($request->has('gambar')) {
            $gambar_post = $request->gambar;
            $gambar_post_baru = time() . $gambar_post->getClientOriginalName();
            $gambar_post->move('public/uploads/post/', $gambar_post_baru);

            $post_data = [
                'judul'         => $request->judul,
                'deskripsi'     => $request->deskripsi,
                'kategori_id'   => $request->kategori_id,
                'tag'           => $request->tag,
                'slug'          => Str::slug($request->judul),
                'gambar'        => 'public/uploads/slider/' . $gambar_post_baru
            ];
        } else {
            $post_data = [
                'judul'         => $request->judul,
                'deskripsi'     => $request->deskripsi,
                'kategori_id'   => $request->kategori_id,
                'tag'           => $request->tag,
                'slug'          => Str::slug($request->judul)
            ];
        }

        $postingan->update($post_data);

        return redirect()->route('postingan.index')->with('toast_success', 'Postingan Berhasil di edit!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $postingan = Postingan::destroy($id);

        return redirect()->route('postingan.index')->with('toast_success', 'Postingan Berhasil di hapus!');
    }
}
