<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Kategori;
use App\Komentar;
use App\Slider;
use App\Postingan;
use Illuminate\Http\Request;
use Faker\Factory as Faker;

class BlogController extends Controller
{
    public function index()
    {
        $postingan = Postingan::orderBy('created_at', 'desc')->paginate(2);
        $slider = Slider::all();
        $kategori = Kategori::all();
        return view('frontend.index', compact('slider', 'postingan', 'kategori'));
    }

    public function detail($slug)
    {
        $postingan = Postingan::where('slug', $slug, 'id')->get();
        $kategori = Kategori::all();
        $komen      = Komentar::all();
        return view('frontend.detail', compact('postingan', 'kategori', 'komen'));
    }

    public function komentar(Request $request, $id)
    {

        $postingan = Postingan::all();

        $this->validate($request, [
            'nama'  => 'required',
            'komen' => 'required'
        ]);

        Komentar::create([
            'postingan_id'  => $request->id,
            'nama'          => $request->nama,
            'komen'         => $request->komen,
        ]);

        return redirect()->back();
    }

    public function kontak()
    {
        return view('frontend.kontak');
    }

    public function cari(Request $request)
    {
        $kategori_cari = Kategori::all();

        $postingan = Postingan::where('judul', $request->cari)->orWhere('judul', 'like', '%' . $request->cari . '%')->paginate(5);

        return view('frontend.index', compact('postingan', 'kategori_cari'));
    }
}
