<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postingan extends Model
{
    protected $table = 'postingan';

    protected $fillable = ['judul', 'deskripsi', 'kategori_id', 'tag', 'users_id', 'slug', 'gambar'];

    public function users()
    {
        return $this->belongsTo('App\User');
    }

    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }

    public function komentar()
    {
        return $this->hasMany('App\Komentar');
    }
}
