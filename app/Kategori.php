<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';

    protected $fillable = ['nama_kategori', 'slug'];

    public function postingan()
    {
        return $this->hasMany('App\Postingan');
    }
}
