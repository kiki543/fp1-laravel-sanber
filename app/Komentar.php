<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $table = 'komentar';

    protected $fillable = ['postingan_id', 'nama', 'komen'];

    public $timestamps = false;
}
