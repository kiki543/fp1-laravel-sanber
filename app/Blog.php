<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    public function slider()
    {
        return $this->belongsTo('App\Slider');
    }

    public function postingan()
    {
        return $this->belongsTo('App\Postingan');
    }

    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }

    public function komentar()
    {
        return $this->hasMany('App\Komentar');
    }
}
